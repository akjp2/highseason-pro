<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       NONE
 * @since      1.0.0
 *
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/admin
 * @author     Dzianis <email@email.email>
 */
class Contact_Form_Messenger_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Contact_Form_Messenger_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Contact_Form_Messenger_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/contact-form-messenger-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Contact_Form_Messenger_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Contact_Form_Messenger_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/contact-form-messenger-admin.js', array( 'jquery' ), $this->version, false );

	}


	    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     */

    public function add_plugin_admin_menu() {

		/*
		 * Add a settings page for this plugin to the Settings menu.
		*/
		add_menu_page( 'Contat Form Messenger', 'CF7 Messenger', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page')
			 );
			 //add_submenu_page ($this->plugin_name, 'Add messengers', 'Add messengers', 'manage_options', $this->plugin_name.'-messengers', array($this, 'display_plugin_settings')
			//);
	   }
   
		/**
		* Add settings action link to the plugins page.
		*/
   
	   public function add_action_links( $links ) {
		   
		  $settings_link = array(
		   '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_name ) . '">' . __('Settings', $this->plugin_name) . '</a>',
		  );
		  return array_merge(  $settings_link, $links );
   
	   }
   
	   /**
		* Render the settings page for this plugin.
		*/
   
	   public function display_plugin_setup_page() {
		   
		   include_once( 'partials/contact-form-messenger-admin-display.php' );
		   
		 } 
		 
		 public function display_plugin_settings() {
		   
			include_once( 'partials/contact-form-messenger-admin-settings.php' );
			
		} 

		/**
		 * Validate options
		 */
		public function validate($input) {
			$options = get_option($this->plugin_name);
			$valid = array();
			if(empty($options)) $options = array();
			if($input['mes-page'] == 'true'){
				$valid = $options;
				if(isset($input['add-bot']) && !empty($input['add-bot'])){
					$valid['bots'][count($valid['bots']) + 1] = array();
				} else 	if(isset($input['delete-bot']) && !empty($input['delete-bot']) && count($valid['bots']) > 0) {
					unset($valid['bots'][count($valid['bots'])]);
				}
				for($count_bots=1;$count_bots <= count($valid['bots']);$count_bots++) {
					$valid['bots'][$count_bots]['messenger'] = (isset($input['messenger-'.$count_bots]) && !empty($input['messenger-'.$count_bots])) ? $input['messenger-'.$count_bots] : '';
					$valid['bots'][$count_bots]['api-token'] = (isset($input['api-token-'.$count_bots]) && !empty($input['api-token-'.$count_bots])) ? $input['api-token-'.$count_bots] : '';
					$valid['bots'][$count_bots]['url'] = (isset($input['url-'.$count_bots]) && !empty($input['url-'.$count_bots])) ? $input['url-'.$count_bots] : '';
					$valid['bots'][$count_bots]['title'] = (isset($input['title-'.$count_bots]) && !empty($input['title-'.$count_bots])) ? $input['title-'.$count_bots] : '';
					$valid['bots'][$count_bots]['bot-id-title'] = (isset($input['title-'.$count_bots]) && !empty($input['title-'.$count_bots])) ? $count_bots.'. '.$input['title-'.$count_bots] : $count_bots.'.';
				}
				return $valid;

			} else if($input['main-page'] == 'true'){
				/*
				$valid = $options;
				for($count_bots=1;$count_bots <= $valid['bots']['amount'];$count_bots++) {
					$valid['main'][$count_bots]['choosen-bot'] = (isset($input['choosen-bot-'.$count_bots]) && !empty($input['choosen-bot-'.$count_bots])) ? $input['choosen-bot-'.$count_bots] : '';				
				}
				for($count_bots=1;$count_bots <= $valid['main']['amount'];$count_bots++) {
					$valid['main'][$count_bots]['form-id'] = (isset($input['form-id-'.$count_bots]) && !empty($input['form-id-'.$count_bots])) ? $input['form-id-'.$count_bots] : '';				
					$valid['main'][$count_bots]['chat-id'] = (isset($input['chat-id-'.$count_bots]) && !empty($input['chat-id-'.$count_bots])) ? $input['chat-id-'.$count_bots] : '';				
				}
				if(isset($input['add-form']) && !empty($input['add-form'])){
					$valid['main']['amount']++;
				} else 	if(isset($input['delete-form']) && !empty($input['delete-form'])){
					$valid['main']['amount']--;
				}
				return $valid;
				*/
				} else {
				return $options;
			}
		}

		/**
		 * Update all options
		 */
		public function options_update() {
				register_setting($this->plugin_name, $this->plugin_name, array($this, 'validate'));
		}

}
