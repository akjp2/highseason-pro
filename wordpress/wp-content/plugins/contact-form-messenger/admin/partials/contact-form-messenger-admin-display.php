<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       NONE
 * @since      1.0.0
 *
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<form method="post" name="my_options" action="options.php">
 
        <?php
        /*
        // Загрузить все значения элементов формы
        $options = get_option($this->plugin_name);

        // текущие состояние опций
        $amount_forms = (isset($options['main']['amount']) && !empty($options['main']['amount'])) ? $options['main']['amount'] : 1;
        $amount = (isset($options['bots']['amount']) && !empty($options['bots']['amount'])) ? $options['bots']['amount'] : 1;
        $add_form = $options['main']['add-form'];
        for($f_i=1;$f_i<=$amount;$f_i++){
            $choosen_bots[$f_i] = $options['bots'][$f_i]['bot-id-title'];
            $choosen_bot[$f_i] = $options['main'][$f_i]['choosen-bot'];
        }

        for($count_bots=1;$count_bots <= $amount_forms;$count_bots++){
            $form_id[$count_bots] = $options['main'][$count_bots]['form-id'];
            $chat_id[$count_bots] = $options['main'][$count_bots]['chat-id'];
        }

        // Выводит скрытые поля формы на странице настроек
        settings_fields( $this->plugin_name );
        do_settings_sections( $this->plugin_name );

        $messengers = array('Facebook','Telegram','VK','Viber');
        */
        $posts = get_posts( array(
            'numberposts' => 5,
            'category'    => 0,
            'orderby'     => 'date',
            'order'       => 'DESC',
            'include'     => array(),
            'exclude'     => array(),
            'meta_key'    => '',
            'meta_value'  =>'',
            'post_type'   => 'bots',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
        ) );   
        ?>
    <h2>Bots' list</h2>

        <fieldset class="fieldset_form_mes">
            <legend class="screen-reader-text"><span><?php _e('Text in the footer', $this->plugin_name);?></span></legend>
            <table class="bots_table">
            <?php if(empty($posts)) echo 'There are no Bots yet. Add them through the wordpress left panel in Bots menu.'; ?>
            <?php foreach($posts as $post) { ?>
            <?php 
                $post_meta = get_post_meta($post->ID);
            ?>
                <tr>        
                <td>
                    <?php 
                        echo 'Bot\'s name: '; 
                        echo '<b>'.$post->post_title.'</b>'; 
                    ?>
                </td>
                <td>
                    <?php 
                        echo 'Messenger: '; 
                        echo $post_meta['selected_bot'][0]; 
                    ?>
                </td>
                <td>
                    <?php 
                        echo 'Title: '; 
                        echo $post_meta['bot_title'][0]; 
                    ?>
                </td>
                <td>
                    <?php 
                        echo 'Token: '; 
                        echo $post_meta['bot_api'][0]; 
                    ?>
                </td>
                </tr>
            <?php }
            wp_reset_postdata(); // сброс
            ?>
            </table>
            <!--
            <input type="submit" 
                    id="<?php echo $this->plugin_name;?>-add-form" 
                    value="<?php esc_attr_e('Add new form', $this->plugin_name);?>" 
                    name="<?php echo $this->plugin_name;?>[add-form]"
                >
                <input type="submit" 
                    id="<?php echo $this->plugin_name;?>-delete-form" 
                    value="<?php esc_attr_e('Delete form', $this->plugin_name);?>" 
                    name="<?php echo $this->plugin_name;?>[delete-form]"
                >
        </fieldset>
        <input
                        class="regular-text-cfm" style="display:none" id="<?php echo $this->plugin_name;?>-main-page"
                        name="<?php echo $this->plugin_name;?>[main-page]"
                            value = "true"
                    >        
        <?php submit_button(__('Save all changes', $this->plugin_name), 'primary','submit', TRUE); ?>
        -->

  </form>