<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       NONE
 * @since      1.0.0
 *
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<form method="post" name="my_options" action="options.php">
 
        <?php

        // Загрузить все значения элементов формы
        $options = get_option($this->plugin_name);
        // текущие состояние опций
        $amount = count($options['bots']);
        for($count_bots=1;$count_bots <= $amount;$count_bots++){
            $messenger[$count_bots] = $options['bots'][$count_bots]['messenger'];
            $token[$count_bots] = $options['bots'][$count_bots]['api-token'];
            $url[$count_bots] = $options['bots'][$count_bots]['url'];
            $title[$count_bots] = $options['bots'][$count_bots]['title'];
        }

        // Выводит скрытые поля формы на странице настроек
        settings_fields( $this->plugin_name );
        do_settings_sections( $this->plugin_name );

        $messengers = array('Facebook','Telegram','VK','Viber');
        
        ?>

    <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

        <fieldset>
            <legend class="screen-reader-text"><span><?php _e('Text in the footer', $this->plugin_name);?></span></legend>
            <table>
            <?php for($count_bots=1;$count_bots <= $amount;$count_bots++) { ?>
            <?php $choosen_mes=''; ?>
                <tr>        
                <td>
                <?php echo $count_bots; ?>
                </td>
                <td>
                <label for="<?php echo $this->plugin_name;?>-messenger-<?php echo $count_bots;?>">
                    <span><?php esc_attr_e('Choose mesenger: ', $this->plugin_name);?></span>
                </label>
                <select
                    class="regular-text-cfm" id="<?php echo $this->plugin_name;?>-messenger-<?php echo $count_bots;?>"
                    name="<?php echo $this->plugin_name;?>[messenger-<?php echo $count_bots;?>]"
                >
                    <?php 
                    for($i=0; $i<count($messengers); $i++){
                        if($i != $messenger[$count_bots]) 
                            echo '<option value="'.$i.'">'.$messengers[$i].'</option>';
                        else {
                            echo '<option selected value="'.$i.'">'.$messengers[$i].'</option>';
                            $choosen_mes = $messengers[$i];
                        }
                    }
                    ?>
                </select>
                </td>
               
                <td>
                <label for="<?php echo $this->plugin_name;?>-api-token-<?php echo $count_bots;?>">
                    <span><?php esc_attr_e('API: ', $this->plugin_name);?></span>
                </label>
                <input
                    class="regular-text-cfm" id="<?php echo $this->plugin_name;?>-api-token-<?php echo $count_bots;?>"
                    name="<?php echo $this->plugin_name;?>[api-token-<?php echo $count_bots;?>]"
                        value = "<?php echo $token[$count_bots];?>"
                >
                </td>

                <td>
                <label for="<?php echo $this->plugin_name;?>-title-<?php echo $count_bots;?>">
                    <span><?php esc_attr_e('Title: ', $this->plugin_name);?></span>
                </label>
                <input
                    class="regular-text-cfm" id="<?php echo $this->plugin_name;?>-title-<?php echo $count_bots;?>"
                    name="<?php echo $this->plugin_name;?>[title-<?php echo $count_bots;?>]"
                        value = "<?php echo $title[$count_bots];?>"
                >
                </td>

                <?php if($choosen_mes == 'Viber') { ?>
                    <td>
                    <label for="<?php echo $this->plugin_name;?>-url-<?php echo $count_bots;?>">
                        <span><?php esc_attr_e('URL: ', $this->plugin_name);?></span>
                    </label>
                    <input
                        class="regular-text-cfm" id="<?php echo $this->plugin_name;?>-url-<?php echo $count_bots;?>"
                        name="<?php echo $this->plugin_name;?>[url-<?php echo $count_bots;?>]"
                            value = "<?php echo $url[$count_bots];?>"
                    >
                    </td>
                <?php } ?>
                </tr>
            <?php } ?>
            </table>
            <input type="submit" 
                    id="<?php echo $this->plugin_name;?>-add-bot" 
                    value="<?php esc_attr_e('Add new bot', $this->plugin_name);?>" 
                    name="<?php echo $this->plugin_name;?>[add-bot]"
                >
                <?php if($amount > 0) { ?>
                <input type="submit" 
                    id="<?php echo $this->plugin_name;?>-delete-bot" 
                    value="<?php esc_attr_e('Delete bot', $this->plugin_name);?>" 
                    name="<?php echo $this->plugin_name;?>[delete-bot]"
                >
                <?php } ?>
        </fieldset>


        <input
                        class="regular-text-cfm" style="display:none" id="<?php echo $this->plugin_name;?>-mes-page"
                        name="<?php echo $this->plugin_name;?>[mes-page]"
                            value = "true"
                    >           
        <?php submit_button(__('Save all changes', $this->plugin_name), 'primary','submit', TRUE); ?>

  </form>