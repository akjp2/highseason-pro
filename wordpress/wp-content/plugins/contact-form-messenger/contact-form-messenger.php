<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              NONE
 * @since             1.0.0
 * @package           Contact_Form_Messenger
 *
 * @wordpress-plugin
 * Plugin Name:       Contact Form 7 Messenger
 * Plugin URI:        NONE
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Dzianis
 * Author URI:        NONE
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       contact-form-messenger
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CONTACT_FORM_MESSENGER_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-contact-form-messenger-activator.php
 */
function activate_contact_form_messenger() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-contact-form-messenger-activator.php';
	Contact_Form_Messenger_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-contact-form-messenger-deactivator.php
 */
function deactivate_contact_form_messenger() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-contact-form-messenger-deactivator.php';
	Contact_Form_Messenger_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_contact_form_messenger' );
register_deactivation_hook( __FILE__, 'deactivate_contact_form_messenger' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-contact-form-messenger.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_contact_form_messenger() {

	$plugin = new Contact_Form_Messenger();
	$plugin->run();

}
run_contact_form_messenger();
