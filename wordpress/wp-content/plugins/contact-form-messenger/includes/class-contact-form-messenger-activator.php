<?php

/**
 * Fired during plugin activation
 *
 * @link       NONE
 * @since      1.0.0
 *
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/includes
 * @author     Dzianis <email@email.email>
 */
class Contact_Form_Messenger_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
