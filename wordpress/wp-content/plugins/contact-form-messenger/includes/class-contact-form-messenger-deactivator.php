<?php

/**
 * Fired during plugin deactivation
 *
 * @link       NONE
 * @since      1.0.0
 *
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/includes
 * @author     Dzianis <email@email.email>
 */
class Contact_Form_Messenger_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
