<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       NONE
 * @since      1.0.0
 *
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/includes
 * @author     Dzianis <email@email.email>
 */
class Contact_Form_Messenger {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Contact_Form_Messenger_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'CONTACT_FORM_MESSENGER_VERSION' ) ) {
			$this->version = CONTACT_FORM_MESSENGER_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'contact-form-messenger';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Contact_Form_Messenger_Loader. Orchestrates the hooks of the plugin.
	 * - Contact_Form_Messenger_i18n. Defines internationalization functionality.
	 * - Contact_Form_Messenger_Admin. Defines all hooks for the admin area.
	 * - Contact_Form_Messenger_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-contact-form-messenger-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-contact-form-messenger-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-contact-form-messenger-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-contact-form-messenger-public.php';

		$this->loader = new Contact_Form_Messenger_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Contact_Form_Messenger_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Contact_Form_Messenger_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Contact_Form_Messenger_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );


		// Add menu item
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_plugin_admin_menu' );

		// Add Settings link to the plugin
		$plugin_basename = plugin_basename( plugin_dir_path( __DIR__ ) . $this->plugin_name . '.php' );
		$this->loader->add_filter( 'plugin_action_links_' . $plugin_basename, $plugin_admin, 'add_action_links' );

		// Save/Update our plugin options
		$this->loader->add_action('admin_init', $plugin_admin, 'options_update');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
		

		$plugin_public = new Contact_Form_Messenger_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		add_action("wpcf7_before_send_mail", array($this,"wpcf7_mess_send")); 
		add_action( 'init', array($this,"wpt_event_post_type") );
		add_action( 'save_post', array($this,"wpt_save_events_meta"), 1, 2 );

		add_action( 'wpcf7_editor_panels', array( $this, 'wpcf7_add_panel' ) );
		add_action( 'wpcf7_after_save', array( $this, 'wpcf7_store_meta' ) );

		add_action('save_post',array($this,'save_post_callback'), 10, 3);
		add_action( 'admin_notices', array($this,'my_admin_notices') );
		add_action(
			'template_redirect',
			function () {
				if (is_singular('bots')) {
				   global $wp_query;
				   $wp_query->posts = [];
				   $wp_query->post = null;
				   $wp_query->set_404();
				   status_header(404);
				   nocache_headers();
				}
			}
		);

		add_filter( 'manage_bots_posts_columns', array($this,'cf7m_add_column'), 10, 0);
		add_filter( 'manage_bots_posts_custom_column', array($this,'cf7m_change_column'), 10, 2);

		add_filter( 'get_user_option_meta-box-order_bots', array($this,'metabox_order' ));
		
	}

	function cf7m_add_column()
	{
		$columns = array(
			'cb' => $columns['cb'],
			'title' => 'Title',
			'messenger' => 'Messenger',
			'date' => 'Date' ,
		  );
		  return $columns;
	}

	function cf7m_change_column( $column, $post_id ){
		// Image column
		if ( 'messenger' == $column ) {
			echo get_post_meta( $post_id, 'selected_bot', true );
		}
	}

	function save_post_callback($post_id){
		global $post; 
		if ($post->post_type != 'bots'){
			return;
		}
		$bot_id = get_post_meta( $post->ID, 'selected_bot', true );
		$api = get_post_meta( $post->ID, 'bot_api', true );
		if ($bot_id == "Telegram") {
			$info = @file_get_contents('https://api.telegram.org/bot'.$api.'/setWebhook?url=https://us-central1-cf7-messager.cloudfunctions.net/telegram?token='.$api);
			if($info == false || json_decode($info, true)['ok'] != 1 ) {
				add_filter( 'redirect_post_location', array($this,'wrong_api_note'), 99 );
			}
		} else if ($bot_id == "Viber") {
			
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://chatapi.viber.com/pa/set_webhook",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "{ \r\n \"url\": \"https://us-central1-cf7-messager.cloudfunctions.net/viber?token=".$api."\",\r\n \"event_types\":[\r\n \"conversation_started\"\r\n ]\r\n}",
				CURLOPT_HTTPHEADER => array(
					"Cache-Control: no-cache",
					"Content-Type: application/JSON",
					"X-Viber-Auth-Token: ".$api
				),
			));	
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err || $response =='' || json_decode($response, true)['status'] != 0) {
				add_filter( 'redirect_post_location', array($this,'wrong_api_note'), 99 );
			}
		}
	}

	function wrong_api_note( $location ) {
		remove_filter( 'redirect_post_location', 'wrong_api_note', 99 );
		return add_query_arg( array( 'api_error' => 'Wrong%20token' ), $location );
	}

	function my_admin_notices() {
		if ( ! isset($_GET['api_error']) )
		 return;
		delete_post_meta($_GET['post'],'bot_api');
		echo '<div class="error"><p>'. esc_html($_GET['api_error']) .'</p></div>';
	}

	function wpt_event_post_type() {
		$labels = array(
			'name'               => __( 'Bots' ),
			'singular_name'      => __( 'Bot' ),
			'add_new'            => __( 'Add New Bot' ),
			'add_new_item'       => __( 'Add New Bot' ),
			'edit_item'          => __( 'Edit Bot' ),
			'new_item'           => __( 'Add New Bot' ),
			'view_item'          => __( 'View Bot' ),
			'search_items'       => __( 'Search Bot' ),
			'not_found'          => __( 'No bots found' ),
			'not_found_in_trash' => __( 'No bots found in trash' )
		);
		$supports = array(
			'title',
		);
		$args = array(
			'labels'               => $labels,
			'supports'             => $supports,
			'exclude_from_search' => true,
			'public'               => true,
			'capability_type'      => 'post',
			'rewrite'              => array( 'slug' => 'bots' ),
			'has_archive'          => false,
			'publicaly_queryable'  => false,
			'query_var' 		   => false,
			'hierarchical' => true,
			'menu_position'        => 30,
			'menu_icon'            => 'dashicons-businessman',
			'register_meta_box_cb' => array($this,"wpt_add_notifications_bots_metaboxes"),
		);
		register_post_type( 'bots', $args );
		

		$labels_2 = array(
			'name'               => __( 'Notifications ' ),
			'singular_name'      => __( 'Notification' ),
			'add_new'            => __( 'Add New Notification' ),
			'add_new_item'       => __( 'Add New Notification' ),
			'edit_item'          => __( 'Edit Notification' ),
			'new_item'           => __( 'Add New Notification' ),
			'view_item'          => __( 'View Notification' ),
			'search_items'       => __( 'Search Notification' ),
			'not_found'          => __( 'No Notification found' ),
			'not_found_in_trash' => __( 'No Notification found in trash' )
		);
		/*
		$supports_2 = array(
			'title',
		);
		$args_2 = array(
			'labels'               => $labels_2,
			'supports'             => $supports_2,
			'public'               => true,
			'exclude_from_search' => true,
			'capability_type'      => 'post',
			'rewrite'              => array( 'slug' => 'notifications'),
			'has_archive'          => false,
			'publicaly_queryable'  => false,
			'query_var' 		   => false,
			'menu_position'        => 30,
			'menu_icon'            => 'dashicons-email-alt',
			'register_meta_box_cb' => array($this,"wpt_add_notifications_bots_metaboxes"),
		);
		register_post_type( 'notifications', $args_2 );
		*/
	}

	/**
	 * Adds a metabox to the right side of the screen under the â€œPublishâ€ box
	 */
	function wpt_add_notifications_bots_metaboxes() {
		/*
		add_meta_box(
			"wpt_bot_id",
			'Bot ID',
			array($this,"wpt_bot_id"),
			'notifications',
			'normal',
			'default'
		);
		add_meta_box(
			"wpt_form_id",
			'Contact Form ID',
			array($this,"wpt_form_id"),
			'notifications',
			'normal',
			'default'
		);
		add_meta_box(
			"wpt_chat_id",
			'Chat ID',
			array($this,"wpt_chat_id"),
			'notifications',
			'normal',
			'default'
		);
		*/
		add_meta_box(
			"wpt_messenger_select",
			'Messenger',
			array($this,"wpt_messenger_select"),
			'bots',
			'normal',
			'default'
		);
		add_meta_box(
			"wpt_bot_title",
			'Bot Name',
			array($this,"wpt_bot_title"),
			'bots',
			'normal',
			'default'
		);
		add_meta_box(
			"wpt_bot_api",
			'Token',
			array($this,"wpt_bot_api"),
			'bots',
			'normal',
			'default'
		);
	}
	
	function metabox_order( $order ) {
		
		return array(
			'normal' => join( 
				",", 
				array(       // vvv  Arrange here as you desire
					'customdiv-bots',
					'authordiv',
					'slugdiv',
				)
			),
		);
	}
	
	/**
	 * Output the HTML for the metabox.
	 */
	function wpt_bot_id() {
		global $post;
		// Nonce field to validate form request came from current site
		wp_nonce_field( basename( __FILE__ ), 'bot_id_fields' );
		// Get the location data if it's already been entered
		$bot_id = get_post_meta( $post->ID, 'bot_id', true );
		// Output the field
		$posts_b = get_posts( array(
            'numberposts' => -1,
            'category'    => 0,
            'orderby'     => 'date',
            'order'       => 'DESC',
            'include'     => array(),
            'exclude'     => array(),
            'meta_key'    => '',
            'meta_value'  =>'',
            'post_type'   => 'bots',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
        ) );
        foreach( $posts_b as $post_b ){
            $bots[] = $post_b->post_title;
        }
        wp_reset_postdata(); // сброс
		foreach($bots as $op){
			if($op == '') continue; 
			$bot_id_ar[] = $op;
		}
		echo '<select name="bot_id">';
		foreach($bot_id_ar as $b){
			if($bot_id == $b )
				echo '<option selected>' . esc_textarea( $b )  . '</option>';
			else
				echo '<option>' . esc_textarea( $b )  . '</option>';
		}
		echo '</select>';
	}
	function wpt_chat_id() {
		global $post;
		// Nonce field to validate form request came from current site
		wp_nonce_field( basename( __FILE__ ), 'chat_id_fields' );
		// Get the location data if it's already been entered
		$chat_id = get_post_meta( $post->ID, 'chat_id', true );
		// Output the field
		echo '<input type="text" name="chat_id" value="' . esc_textarea( $chat_id )  . '" class="widefat">';
	}
	function wpt_form_id() {
		$args = array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => -1);
		$cf7Forms = get_posts( $args );
		// $post_ids = wp_list_pluck( $cf7Forms , 'ID' );
		$form_titles = wp_list_pluck( $cf7Forms , 'post_title' );
		global $post;
		// Nonce field to validate form request came from current site
		wp_nonce_field( basename( __FILE__ ), 'form_id_fields' );
		// Get the location data if it's already been entered
		$form_id = get_post_meta( $post->ID, 'form_id', true );
		// Output the field
		echo '<select name="form_id">';
		foreach($form_titles as $b){
			if($form_id == $b )
				echo '<option selected>' . esc_textarea( $b )  . '</option>';
			else
				echo '<option>' . esc_textarea( $b )  . '</option>';
		}
		echo '</select>';
	}
	function wpt_messenger_select() {
		global $post;
		// Nonce field to validate form request came from current site
		wp_nonce_field( basename( __FILE__ ), 'messengers_select_fields' );
		// Get the location data if it's already been entered
		$selected_bot = get_post_meta( $post->ID, 'selected_bot', true );
		// Output the field
		$bot_id_ar = array('Facebook','Telegram','VK','Viber');
		echo '<b>Bot</b> <select name="selected_bot">';
		foreach($bot_id_ar as $b){
			if($selected_bot == $b )
				echo '<option selected>' . esc_textarea( $b )  . '</option>';
			else
				echo '<option>' . esc_textarea( $b )  . '</option>';
		}
		echo '</select>';
	}
	function wpt_bot_title() {
		global $post;
		// Nonce field to validate form request came from current site
		wp_nonce_field( basename( __FILE__ ), 'bot_title_fields' );
		// Get the location data if it's already been entered
		$bot_title = get_post_meta( $post->ID, 'bot_title', true );
		// Output the field
		echo '<input type="text" name="bot_title" value="' . esc_textarea( $bot_title )  . '" class="widefat">';
	}
	function wpt_bot_api() {
		global $post;
		// Nonce field to validate form request came from current site
		wp_nonce_field( basename( __FILE__ ), 'bot_api_fields' );
		// Get the location data if it's already been entered
		$bot_api = get_post_meta( $post->ID, 'bot_api', true );
		// Output the field
		echo '<input type="text" name="bot_api" value="' . esc_textarea( $bot_api )  . '" class="widefat">';
	}

	/**
	 * Save the metabox data
	 */
	function wpt_save_events_meta( $post_id, $post ) {
		// Return if the user doesn't have edit permissions.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
		// Verify this came from the our screen and with proper authorization,
		// because save_post can be triggered at other times.
		if ( ! isset( $_POST['bot_id'] ) && ! isset( $_POST['form_id'] ) && ! isset( $_POST['chat_id']) && ! wp_verify_nonce( $_POST['form_id_fields'], basename(__FILE__) ) ) {
			//return $post_id;
		}
		// Now that we're authenticated, time to save the data.
		// This sanitizes the data from the field and saves it into an array $events_meta.
		$events_meta['bot_id'] = esc_textarea( $_POST['bot_id'] );
		$events_meta['form_id'] = esc_textarea( $_POST['form_id'] );
		$events_meta['chat_id'] = esc_textarea( $_POST['chat_id'] );
		$events_meta['selected_bot'] = esc_textarea( $_POST['selected_bot'] );
		$events_meta['bot_api'] = esc_textarea( $_POST['bot_api'] );
		$events_meta['bot_title'] = esc_textarea( $_POST['bot_title'] );
		// Cycle through the $events_meta array.
		// Note, in this example we just have one item, but this is helpful if you have multiple.
		foreach ( $events_meta as $key => $value ) :
			// Don't store custom data twice
			if ( 'revision' === $post->post_type ) {
				return;
			}
			if ( get_post_meta( $post_id, $key, false ) ) {
				// If the custom field already has a value, update it.
				update_post_meta( $post_id, $key, $value );
			} else {
				// If the custom field doesn't have a value, add it.
				add_post_meta( $post_id, $key, $value);
			}
			if ( ! $value ) {
				// Delete the meta key if there's no value
				delete_post_meta( $post_id, $key );
			}
		endforeach;
	}

	public function wpcf7_mess_send($WPCF7_ContactForm) {
			//Get current form
			$wpcf7      = WPCF7_ContactForm::get_current();
			// get current SUBMISSION instance
			$submission = WPCF7_Submission::get_instance();
			// Ok go forward
			if ($submission) {
				// get submission data
				$data = $submission->get_posted_data();
				// nothing's here... do nothing...
				$id = $data['_wpcf7'];
				if (empty($data))
					return;
				foreach($data as $key => $d){
					if(strpos($key,'wpcf7') === false) $information[$key] = $d;
				}
				$fields = $this->get_fields_values( $id );
				$fields = $this->make_array($fields);
				if(empty($fields)) return $wpcf7;	
				foreach($fields as $field){
					if(get_post_meta($field['page_id_'],'selected_bot',true) == '' || get_post_meta($field['page_id_'],'bot_api',true) == '') continue;
					$body = $WPCF7_ContactForm->filter_message($field['textarea-message_']);
					$curl = curl_init();
					$data = array('botName' => html_entity_decode(get_the_title($id)), 'botMessenger' => html_entity_decode(strtolower(get_post_meta($field['page_id_'],'selected_bot',true))), 'chatId' => html_entity_decode($field['chat_id_']), 'message' => html_entity_decode($body));
					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://us-central1-cf7-messager.cloudfunctions.net/sendMessage?token=".get_post_meta($field['page_id_'],'bot_api',true),
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => json_encode($data),
						CURLOPT_HTTPHEADER => array(
							"Cache-Control: no-cache",
							"Content-Type: application/JSON",
						),
					));	
					$response = curl_exec($curl);
					$err = curl_error($curl);
					curl_close($curl);
				}			
				/*// extract posted data for example to get name and change it
				$name         = isset($data['your-name']) ? $data['your-name'] : "";
				// do some replacements in the cf7 email body
				$mail         = $wpcf7->prop('mail');*/
			}
		return $wpcf7;
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Contact_Form_Messenger_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}



	public function wpcf7_add_panel( $panels ) {
		$panels['messenger-panel'] = array(
			'title'     => __( 'Messengers Integrations', 'wpcf7-messenger' ),
			'callback'  => array( $this, 'create_panel_inputs' ),
		);
		return $panels;
	}

	public function create_panel_inputs( $post ) {
		wp_nonce_field( 'wpcf7_messenger_page_metaboxes', 'wpcf7_messenger_page_metaboxes_nonce' );
		$fields = $this->get_fields_values( $post->id());
		foreach($fields as $key => $field){
			if(strpos($key,'page_id_') !== false) $num_ar[] = str_replace('page_id_','',$key);
			else continue;
		}	
		if($num_ar) $k = max($num_ar);
		else $k = -1;
		?>
		<h2>
			<?php esc_html_e( 'CF7 Messenger Settings', 'wpcf7-messenger' );?>
		</h2>
		<?php for ($i=0;$i<=$k;$i++) { ?>
			<fieldset class="cf7-messenger-fieldset">
				<table class="form-table">
					<tr class="field-wrap field-wrap-page-id">
						<th scope="row">
							<label for="wpcf7-messenger-page-id-<?php echo $i;?>">
								<b><?php esc_html_e( 'Bot', 'wpcf7-messenger' );?></b> 
							</label>
						</th>
						<td>
							<?php
							echo wp_dropdown_pages( array(
									'echo'              => 0,
									'name'              => 'wpcf7-messenger[page_id_'.$i.']',
									'show_option_none'  => __( 'Choose Bot', 'wpcf7-messenger' ),
									'option_none_value' => '0',
									'selected'          => $fields['page_id_'.$i],
									'id'                => 'wpcf7-messenger-page-id-'.$i,
									'class'				=> 'wpcf7-messenger-page-id',
									'post_type' => 'bots'
								)
							);
							?>   
						</td>
						<td>
							<button class="wpcf7-messenger-button" id="wpcf7-messenger-button-bot-button" name="wpcf7-messenger[button-bot]" value="Delete Notification №<?php echo $i; ?>" type="submit">Remove the Notification</button>     
						</td>
				</tr>
				<tr class="field-wrap field-wrap-chat-id">
					<th>
						<label for="wpcf7-messenger-chat-id-<?php echo $i;?>">
							<b><?php esc_html_e( 'Chat ID', 'wpcf7-messenger' );?></b>
						</label>
					</th>
					<td>
						<input type="text" id="wpcf7-messenger-chat-id-<?php echo $i;?>" name="wpcf7-messenger[chat_id_<?php echo $i; ?>]" value="<?php echo $fields['chat_id_'.$i];?>">
					</td>		
				</tr>
				<tr class="field-wrap field-wrap-after-sent-script">
					<th>
						<label for="wpcf7-messenger-textarea-message-<?php echo $i;?>">
							<b><?php esc_html_e( 'Message', 'wpcf7-messenger' );?></b>
						</label>
					</th>
					<td>
						<span class='textarea_text'>In the following field, you can use <a href="//contactform7.com/special-mail-tags/" target="_blank">special mail-tags</a> or your mail-tags:<br>
						<b><?php $post->suggest_mail_tags( $args['name'] ); 					
						?></b></span>
						<br>
						<textarea id="wpcf7-messenger-textarea-message-<?php echo $i;?>" name="wpcf7-messenger[textarea-message_<?php echo $i; ?>]" rows="8" cols="100"><?php echo $fields['textarea-message_'.$i];?></textarea>
					</td>
				</tr>
				</table>
				<hr>
			</fieldset>
		<?php } ?>
		<input type="submit" class="button_add_not" id="wpcf7-messenger-button-bot-button" name="wpcf7-messenger[button-bot]" value="Add Notification">
		<script>
			(function($){
					$( '#wpcf7-admin-form-element :input[type!="hidden"]' ).each( function() {
						if ( $( this ).is( ':checkbox, :radio' ) ) {
							this.defaultChecked = $( this ).is( ':checked' );
						} else if ( $( this ).is( 'select' ) ) {
							$( this ).find( 'option' ).each( function() {
								this.defaultSelected = $( this ).is( ':selected' );
							} );
						} else {
							this.defaultValue = $( this ).val();
						}
					});
			})(jQuery);
		</script>
		<?php
	}

	public function get_fields_values( $post_id ) {
		$i = 0;
		while(get_post_meta( $post_id, '_wpcf7_messenger_page_id_'.$i , true ) !== ''){
			$i++;
		}
		$i = $i - 1;
		if($i<0) return array();
		$fields = $this->get_plugin_fields($i);
		if(empty($fields)) return;
		foreach ( $fields as $field ) {
			$k = $i;
			while($k >= 0){
				$values[ $field['name'] . $k ] = get_post_meta( $post_id, '_wpcf7_messenger_' . $field['name'] . $k , true );
				$k-- ;
			}
		}
		return $values;
	}

	public function make_array($array){
		$new_ar = array();
		$other_k = array();
		foreach($array as $k => $ar){
			if(strpos($k,'button-bot') !== false) { unset($array[$k]); continue; }
			if(preg_replace('/[^0-9]*/','',$k) === '') $other_k[$k] = $ar;
			else {
				$new_ar[preg_replace('/[^0-9]*/','',$k)][preg_replace('/[0-9]*/','',$k)] = $ar;
			}
		}
		return array_merge($new_ar,$other_k);
	}

	public function wpcf7_store_meta( $contact_form ) {
		if ( ! isset( $_POST['wpcf7-messenger'] ) || empty( $_POST['wpcf7-messenger'] ) ) {
			return;
		} else {
			if ( ! wp_verify_nonce( $_POST['wpcf7_messenger_page_metaboxes_nonce'], 'wpcf7_messenger_page_metaboxes' ) ) {
				return;
			}
			
			$i = -1;
			$add_bot = false;
			foreach($_POST['wpcf7-messenger'] as $k => $f){
				if(strpos($k,'page_id') !== false) {
					$i++;
				}
				if($k == 'button-bot' && $f == 'Add Notification') {
					$add_bot = 'add';
				}
				else if($k == 'button-bot' && strpos($f,'Delete Notification') !== false){
					$add_bot = $f;
					$delete_bot_n = str_replace('Delete Notification №', '' , $add_bot);
				}
			}
			$form_id        = $contact_form->id();
			$fields         = $this->get_plugin_fields( $i , $add_bot );
			$data           = $_POST['wpcf7-messenger'];
			$data_ar = $this->make_array($data);

			if(isset($delete_bot_n)){
				if(!isset($data_ar[$delete_bot_n + 1])){
					foreach($data_ar[$delete_bot_n] as $k => $d){
						delete_post_meta( $form_id, '_wpcf7_messenger_' . $k . $delete_bot_n);
					}
					unset($data_ar[$delete_bot_n]);
				} else {
					$data_ar[$delete_bot_n] = 'false';
					while($data_ar[$delete_bot_n] == 'false'){
						if(isset($data_ar[$delete_bot_n + 1]) && isset($data_ar[$delete_bot_n + 2])) {
							$data_ar[$delete_bot_n] = $data_ar[$delete_bot_n + 1];
							$data_ar[$delete_bot_n + 1] = 'false';
						}
						else if(isset($data_ar[$delete_bot_n + 1]) && !isset($data_ar[$delete_bot_n + 2])) {
							$data_ar[$delete_bot_n] = $data_ar[$delete_bot_n + 1];
							foreach($data_ar[$delete_bot_n + 1] as $k => $d){
								delete_post_meta( $form_id, '_wpcf7_messenger_' . $k . ($delete_bot_n + 1));
							}
							unset($data_ar[$delete_bot_n + 1]);
						}
						$delete_bot_n++;
					}
				}
				//var_dump( get_post_meta( $form_id, '_wpcf7_messenger_page_id_7', true) );	
			} else if($add_bot == 'add'){
				$i_ar = count($data_ar) - 1;
					foreach($fields as $field){
						$data_ar[$i_ar][$field['name']] = '';
					}
				}
				foreach ( $data_ar as $k => $data ) {
					if(preg_replace('/[0-9]*/','',$k) != '') continue;
					foreach($fields as $y => $field) { 
						$value = isset( $data[ $field['name'] ] ) ? $data[ $field['name'] ] : ' ';
						switch ( $field['type'] ) {
							case 'text':
							case 'checkbox':
								$value = sanitize_text_field( $value );
								$name = $field['name'] . $k;
								break;
							case 'textarea':
								$value = htmlspecialchars( $value );
								$name = $field['name'] . $k;
								break;
							case 'submit':
								$name = false;
								continue;
								break;
							case 'number':
								$value = intval( $value );
								$name = $field['name'] . $k;
								break;
							case 'url':
								$value = esc_url_raw( $value );
								$name = $field['name'] . $k;
								break;
						}
						/*if(strpos($add_bot,'Delete Notification') !== false && str_replace('Delete Notification №', '' , $add_bot) == $k) {
							delete_post_meta( $form_id, '_wpcf7_messenger_' . $field['name'], $value );
						}
						else */ 
						if($name != false) {
							update_post_meta( $form_id, '_wpcf7_messenger_' . $name, $value );
						}
					}
				}
		}
	}

	public function get_plugin_fields( $i=0 , $add_bot = false ) {
			$fields = array(
				array(
					'name' => 'page_id_',
					'type' => 'number',
				),
				array(
					'name' => 'chat_id_',
					'type' => 'text',
				),
				array(
					'name' => 'textarea-message_',
					'type' => 'textarea',
				),
				array(
					'name' => 'button-bot',
					'type' => 'submit',
				),
			);
		return $fields;
	}
}
