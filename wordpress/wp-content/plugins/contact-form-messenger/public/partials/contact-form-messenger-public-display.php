<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       NONE
 * @since      1.0.0
 *
 * @package    Contact_Form_Messenger
 * @subpackage Contact_Form_Messenger/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
