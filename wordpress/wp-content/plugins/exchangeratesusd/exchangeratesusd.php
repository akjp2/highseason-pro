<?php
/*******
 * Plugin Name: Exchange Rates USD
 * Description: Load USD Rates from openexchangerates.org
 * Version: 1.0
 * Author: Kanthrad
 * Text Domain: openexchangerates
 *******/
// Кэширование во временные опции
$rates = array();
$transient = 'currency'; // название опции
$currencys = get_transient($transient); // получение данных, если сохранены
if(!$currencys){ // создание (обновление) данных, если нет (просрочены)
	$url = 'https://openexchangerates.org/api/latest.json?app_id=8ee6632dfb014af09077c7b9830f44e7&base=USD';
	$time = WEEK_IN_SECONDS; // время жизни - неделя
	$response = wp_safe_remote_get($url, array());
	// проверка и получение тела ответа
	if(wp_remote_retrieve_response_code($response) === 200){
		$data = wp_remote_retrieve_body($response);
		$data = json_decode($data, true);
		$byn = $data['rates']['BYN'];
		$rub = $data['rates']['RUB'];
		$uah = $data['rates']['UAH'];
		$eur = $data['rates']['EUR'];
		$rates = array('byn' => $byn, 'rub' => $rub, 'uah' => $uah, 'eur' => $eur);
		
	}else{
		$rates = array('byn' => '', 'rub' => '', 'uah' => '', 'eur' => '');
	}
	// сохранение курсов во временную опцию
	set_transient( $transient, $rates, $time );
}

add_filter('get_post_metadata', 'convert_price', 10, 4);
function convert_price($metadata, $object_id, $meta_key, $single){
	if(!is_blog_admin()){
		if ( isset( $meta_key) && $meta_key == 'price_solutions' ){
			$currencys = get_transient('currency'); // получение данных, если сохранены
			if($currencys){
				remove_filter('get_post_metadata', 'convert_price', 10, 4);
				$convert_meta = get_metadata( 'post', $object_id, $meta_key, true);
		        add_filter('get_post_metadata', 'convert_price', 10, 4);

		        if (function_exists('geoip_detect2_get_info_from_ip')) {
					$userIP = $_SERVER['HTTP_CLIENT_IP'] ? $_SERVER['HTTP_CLIENT_IP'] : ($_SERVER['HTTP_X_FORWARDED_FOR'] ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);
					$userInfo = geoip_detect2_get_info_from_ip($userIP, NULL);
					$isoCode = $userInfo->country->isoCode;
					
					if ($isoCode == 'BY') {
						$convert_meta = $convert_meta * $currencys['byn'];
						$convert_meta = number_format($convert_meta, 2, '.', ' ') . ' BYN';

					} elseif ($isoCode == 'RU') {
						$convert_meta = $convert_meta * $currencys['rub'];
						$convert_meta = number_format($convert_meta, 2, '.', ' ') . ' RUB';

					} elseif ($isoCode == 'UA') {
						$convert_meta = $convert_meta * $currencys['uah'];
						$convert_meta = number_format($convert_meta, 2, '.', ' ') . ' UAH';
						
					} elseif ($isoCode == 'LT' || $isoCode == 'LV' || $isoCode == ' DE' || $isoCode == 'IT' || $isoCode == 'CY' || $isoCode == 'BG') {
						$convert_meta = $convert_meta * $currencys['eur'];
						$convert_meta = number_format($convert_meta, 2, '.', ' ')  . ' EUR';
						
					} else {
						$convert_meta = $convert_meta . ' $';
					}		
				}

				return array($convert_meta);     
			}
		}
	}
	return $metadata;
}