<?php get_header(); ?>
<div class="container center">
	<div class="main">
		<?php the_archive_title('<div class="entry"><h1 class="entry__title">', '</h1></div>'); ?>
		<?php
			if ( have_posts() ) : ?>
			<?php
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content/content', get_post_type() );
				endwhile;
			else :
				get_template_part( 'template-parts/content/content', 'none' );
			endif;
		?>
	</div>
</div>
<?php get_footer(); ?>