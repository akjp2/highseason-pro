<?php
/**
 * HighSeason Theme functions
 *
 * @package chart
 */

if ( ! function_exists( 'highseason_setup' ) ) :
	/**
	 * Устанавливает настройки темы по умолчанию и регистрирует функции WordPress, которые
	 * тема поддерживает
	 */
	function highseason_setup() {
		load_theme_textdomain( 'chart' ); // Загружает файл перевода темы (.mo)
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'cpt_thumb', 300, 170, true );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
	}
endif; // end function_exists highseason_setup.
add_action( 'after_setup_theme', 'highseason_setup' );


/************************************************************
 * СТРАНИЦА НАСТРОЕК ТЕМЫ
 ************************************************************/

// Регистрация скриптов

function include_adminthemescript() {
	// wp_enqueue_script('jquery');
	// скрипты и стили загрузчика изображений WordPress
	if ( ! did_action( 'wp_enqueue_media' ) ) {
		wp_enqueue_media();
	}
 	wp_enqueue_script( 'adminthemescript', get_stylesheet_directory_uri() . '/js/admin-theme.js', array('jquery'), null, false );
} 
add_action( 'admin_enqueue_scripts', 'include_adminthemescript' );

function cptui_styles() {
    if ( is_front_page() || is_post_type_archive( 'archive-ideas.php' || 'archive-solutions.php' ) ) {
        wp_enqueue_style( 'cptui', get_template_directory_uri() . '/css/cptui.css' );
        wp_enqueue_script( 'cptui', get_template_directory_uri() . '/js/cptui.js' );
    }
}
add_action( 'wp_enqueue_scripts', 'cptui_styles' );

// Настройка и вывод панели администрирования

function theme_setting(){
?>
	<div class="wrap">
		<h2><?php echo get_admin_page_title(); ?></h2>
		<form action="options.php" method="POST" enctype="multipart/form-data">
			<?php
				settings_fields('option_group');     // скрытые защитные поля
				do_settings_sections('option_page'); // секции с настройками (опциями).
				submit_button();
			?>
		</form>
	</div>
<?php
}
add_action('admin_menu', function(){
	add_menu_page( 'Настройки темы', 'Настройки темы', 'manage_options', 'theme-options', 'theme_setting', '', 4 ); 
} );

// Регистрация настроек

function theme_page_settings(){
	// параметры: $option_group, $option_name, $sanitize_callback
	register_setting( 'option_group', 'theme_option_arr', 'sanitize_callback' );

	// параметры: $id, $title, $callback, $page
	add_settings_section( 'section_id', 'Основные настройки', '', 'option_page' ); 

	// параметры: $id, $title, $callback, $page, $section, $args
	add_settings_field('field_logo', 'Логотип', 'fill_field_logo', 'option_page', 'section_id' );
}
add_action('admin_init', 'theme_page_settings');

// добавление полей
function fill_field_logo(){
		$val = get_option('theme_option_arr');
		$val = $val ? $val['logo'] : null;

		$default = get_stylesheet_directory_uri() . '/images/empty.jpg';

		if($val) {
			$src = wp_get_attachment_image_url($val);
		} else {
			$src = $default;
		}
?>
	<div>
		<img data-src="<?php echo $default; ?>" src="<?php echo $src; ?>" width="115px" height="90px">
		<div>
			<input type="hidden" name="theme_option_arr[logo]" id="theme_option_arr[logo]" value="<?php echo $val; ?>">
			<button type="submit" class="upload_image_button button">Загрузить</button>
			<button type="submit" class="remove_image_button button">&times;</button>
		</div>
	</div>
<?php
}

// Очистка данных
function sanitize_callback( $options ){ 
	foreach( $options as $name => & $val ){
		if( $name == 'logo' )
			$val = strip_tags( $val );
	}
	return $options;
}

/************************************************************
 * МЕНЮ
 ************************************************************/

function theme_register_nav_menu() {
	register_nav_menu( 'top', 'Top Menu' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu' );

// Удаление фильтра autop Contact Form 7
add_action( 'wpcf7_autop_or_not', '__return_false' );

// Удаление префикса заголовка архива
add_filter( 'get_the_archive_title', function( $title ){
	return preg_replace('~^[^:]+: ~', '', $title );
});
