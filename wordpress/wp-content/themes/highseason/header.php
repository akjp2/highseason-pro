<?php
class NavMenu extends Walker_Nav_Menu
{
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0){
        global $wp_query;
        $item_class = in_array("current_page_item", $item->classes) ? 'menu-item menu-item__active' : 'menu-item';
        $output .= '<li class="' . $item_class . '">';
        $attributes = !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
        $item_output = sprintf('<a%1$s>%2$s</a>',$attributes,apply_filters('the_title', $item->title, $item->ID));
        // build html
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
}
?>


<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri() ?>">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
$option_arr = get_option('theme_option_arr');
if ($option_arr['logo'] != '') {
    $logo = $option_arr['logo'];
    $logo = wp_get_attachment_image_url($logo, 'full');
} else {
    $logo = get_stylesheet_directory_uri() . '/images/logo.png';
}
?>

<div class="container center">
    <header class="header">
        <div class="header-logo">
            <a
                    class="logo"
                    href="<?php echo esc_url(home_url('/')); ?>"
                    title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
                    rel="home"
            >
                <img class="logo-img" src="<?php echo $logo; ?>">
            </a>
        </div>
        <div class="header-nav">
            <?php
            wp_nav_menu([
                'theme_location' => '',
                'menu' => '',
                'container' => false,
                'items_wrap' => '<ul class="menu">%3$s</li></ul>',
                'fallback_cb' => '',
                'walker' => new NavMenu
            ]);
            ?>
        </div>
    </header>
</div>