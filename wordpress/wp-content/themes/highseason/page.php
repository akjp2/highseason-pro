<?php get_header(); ?>
<div class="container center">
	<div class="main">
		<?php
			if ( have_posts() ) : ?>
			<?php
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content/content', get_post_type() );
				endwhile;
			else :
				get_template_part( 'template-parts/content/content', 'none' );
			endif;
		?>
	</div>
</div>
<?php get_footer(); ?>