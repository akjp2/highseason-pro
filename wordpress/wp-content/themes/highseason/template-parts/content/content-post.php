<div class="entry">
    <?php
    if (is_singular()) :
        the_title('<h1 class="entry__title">', '</h1>');
    else :
        the_title('<h2 class="entry__title"><a href="' . esc_url(get_permalink()) . '">', '</a></h2>');
    endif;
    ?>
    <div class="entry__content">
        <?php the_content(); ?>
    </div>
</div>