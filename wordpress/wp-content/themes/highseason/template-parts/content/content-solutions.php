<?php if (is_singular()) : ?>
	<div class="entry">
		<h1 class="entry__title"><?php the_title(); ?></h1>
		<div class="entry__content"><?php the_content(); ?></div>
	</div>
<?php else : ?>
	<?php get_template_part( 'template-parts/loop/announcement' ); ?>
<?php endif; ?>