<?php $permalink = esc_url(get_permalink()); ?>
<div class="announcement__item">
	<div class="announcement__wrap">
		<a href="<?php echo $permalink; ?>"><?php the_post_thumbnail( 'cpt_thumb', array('class' => 'announcement__thumb') ); ?></a>
		<div class="announcement__caption">
			<h4 class="announcement__title">
				<a href="<?php echo $permalink; ?>"><?php the_title(); ?></a>
			</h4>
			<div class="announcement__excerpt"><?php the_excerpt(); ?></div>
			<div class="announcement__order">
				<div class="announcement__price">
					<?php if(get_field('price_solutions')): ?>
						<?php echo get_field('price_solutions'); ?>
					<?php endif; ?>
				</div>
				<a class="button announcement__button" href="<?php echo $permalink; ?>">Подробнее</a>
			</div>
		</div>
	</div>
</div>